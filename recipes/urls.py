from django.urls import path


from recipes.views import (
    RecipeCreateView,
    RecipeUpdateView,
    RecipeDetailView,
    log_rating,
    RescipeListView,
)

urlpatterns = [
    path("", RescipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("edit/", RecipeUpdateView, name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
]
